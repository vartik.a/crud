const db=require('../models')

const Table=db.table

//create user

const addUser=async (req,res)=>
{
    let info={
         
        "name":req.body.name,
        "mobile":req.body.mobile,
        "email":req.body.email
    }

    const user=await Table.create(info)
    res.status(200).send(user)
    console.log(user);
}

//get user

const getAllUsers=async (req,res)=>
{

    let users=await Table.findAll({})
    res.status(200).send(users)
}

//update user

const updateUser=async (req,res)=>
{

    let id=req.params.id
    
    const user=await Table.update(req.body,{where:{id:id}})
    res.status(200).send(user);
}

//delete user by id

const deleteUser =async(req,res)=>
{
    let id=req.params.id
    await Table.destroy({where :{id:id}})
    res.status(200).send('User deleted')
}

module.exports=
{
   addUser,
   getAllUsers,
   updateUser,
   deleteUser



}