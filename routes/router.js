const controller=require('../controllers/controller')

const router =require('express').Router()

router.post('/addUser',controller.addUser)
router.get('/allUsers',controller.getAllUsers)
router.put('/:id',controller.updateUser)
router.delete('/:id',controller.deleteUser)

module.exports=router