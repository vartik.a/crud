const express=require("express");
const cors=require("cors")
const body_parser=require("body-parser")

const app=express()
const PORT=process.env.PORT||3000

var corOptions=
{
    origin: 'https://localhost:3000'
}



app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(body_parser.json())

const router=require('./routes/router')
app.use('/api',router)

app.get('/',(req,res)=>
{
    res.json(({message:'hello from api'}))
})

app.listen(PORT,()=>{
    console.log(`server is running on port:${PORT}`)
})
