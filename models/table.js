module.exports = (sequelize, DataTypes) => {

    const Table = sequelize.define("table", {
        id: {
            type: DataTypes.INTEGER,
            allowNull:false,
            autoIncrement:true,
            primaryKey:true

        },

        name: {
            type: DataTypes.STRING,
            allowNull: false
        },

        mobile: {
            type: DataTypes.INTEGER,
            allowNull:false
        },

        email: {
            type: DataTypes.STRING,
            allowNull:false
        },

        updatedAt: {
            type: DataTypes.DATE,
            allowNull:false,
            defaultValue: sequelize.literal("CURRENT_TIMESTAMP")
        },

        createdAt:
        {
            type: DataTypes.DATE,
            allowNull:false,
            defaultValue: sequelize.literal("CURRENT_TIMESTAMP")
        }
    
    })

    return Table;

}